/// <reference types="Cypress" />
describe('My First Test Suite',function()
{
    it('My First Test Case', function()
    {
      cy.visit("https://rahulshettyacademy.com/seleniumPractise/#/") 
      cy.get('.search-keyword').type('ca')
      cy.wait(2000)
      cy.get('.product:visible').should('have.length',4)
      cy.get('.products').find('.product').should('have.length',4)
      cy.get('.products').find('.product')
        .each(($el, index, $list) => 
        {
          const textVeg=$el.find('h4.product-name').text()
          if (textVeg.includes('Cashews'))
            {
              $el.find('button').click()
            }     
        })
        //assert if logo text is displayed
        cy.get('.brand').should('have.text','GREENKART')
        // print the logo text
        cy.get('.brand').then(function(logoelement)
        {
          cy.log(logoelement.text())
        })
    })
})